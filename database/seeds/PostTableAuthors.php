<?php

use Illuminate\Database\Seeder;

class PostTableAuthors extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/*insert ke table authrs*/
        App\Author::create(['name'=>'Aam Amiruddin']);
        App\Author::create(['name'=>'Aam Amiruddin']);

        /*untuk update*/
        $author = App\Author::find(1);
		$author->update(['name'=>'Salim A Fillah']);
    }
}
