<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/about', 'MyController@showAbout');
//Route::get('/test', 'MyController@Showtest');
    

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'web'], function () {	
Route::group(['prefix'=>'admin', 'middleware'=>['auth', 'role:admin']], function() {
	Route::resource('authors', 'AuthorsController');
	Route::resource('books', 'BooksController');
});
});
Route::group(['prefix'=>'admin', 'middleware'=>['auth', 'role:member']], function() {

	Route::resource('heri', 'HeriController');
	Route::get('/test', 'MyController@Showtest');

});