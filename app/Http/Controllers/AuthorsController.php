<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
//  use Session;//untuk flash message
class AuthorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Builder $htmlBuilder)
    {
            if ($request->ajax()) {
                $authors = Author::select(['id', 'name', 'created_at', 'updated_at']);
                //return Datatables::of($authors)->make(true);
                return Datatables::of($authors)
              ->addColumn('action', function($author){
                return view('datatable._action', [//->Menentukan arah actionnya ke view
                'model'           => $author,
                'edit_url'        => route('authors.edit', $author->id),//->untuk Edit
                'form_url'        => route('authors.destroy', $author->id),//->untuk Hapus
                'confirm_message' => 'Yakin mau menghapus ' . $author->name . '?'
                ]);
                })->make(true);
            }

            $html = $htmlBuilder
                  //->addColumn(['data'=>'name', 'name'=>'name', 'title' => 'Nama']);
                    ->addColumn(['data' => 'name', 'name'=>'name', 'title'=>'Nama'])
                    ->addColumn(['data' => 'created_at', 'title'=>'Dibuat Tanggal'])
                    ->addColumn(['data' => 'updated_at', 'title'=>'Diubah Tanggal'])
                    ->addColumn(['data' => 'action', 'name'=>'action', 'title'=>'Aksi', 
                        'orderable'=>false, 'searchable'=>false]);
                  return view('authors.index')->with(compact('html'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('books.create');  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|unique:authors']);
        $author = Author::create($request->all());
        
        Session::flash("flash_notification", [
        "level"=>"success",
        "message"=>"Berhasil menyimpan $author->name" ]);
        return redirect()->route('authors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan di form berdasarkan id
        $author = Author::find($id);
        return view('authors.edit')->with(compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validasi nama
        $this->validate($request, ['name' => 'required|unique:authors,name,'. $id]);
                $author = Author::find($id);
                $author->update($request->only('name'));
                Session::flash("flash_notification", [
                "level"=>"success",
                "message"=>"Berhasil mengedit $author->name"
        ]);
        return redirect()->route('authors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // Author::destroy($id);
        //Jika event hapus bernilai false maka akan di arahkan ke halaman sblumnya
        if(!Author::destroy($id)) return redirect()->back();
        session::flash("flash_notification", ["level"=>"success",
        "message"=>"Penulis Berhasil Di Hapus"
     ]);
        return redirect()->route('authors.index');
    }
}
    