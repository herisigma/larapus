<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;


class Book extends Model
{
	//protected $table ='books';
	
   	//membuat parsing agar file bisa langsung di rubah
   	protected $fillable = ['title', 'author_id', 'amount', 'cover'];
    
    Public function author()
	{
		// model, primary key, froreign key
		return $this->hasOne('App\Author', 'id', 'author_id');
	}
}

